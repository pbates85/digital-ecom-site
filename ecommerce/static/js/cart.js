
// query all cart items with the class name of update cart
var updateBtns = document.getElementsByClassName('update-cart')

//loop through all buttons and add event listeners
for (i=0; i < updateBtns.length; i++) {
    //on click type of event is click and on click set function
    updateBtns[i].addEventListener('click', function(){
        var productId = this.dataset.product //store.html = data-action
        var action = this.dataset.action
        console.log('productId:', productId, 'Action:', action)
        // console.log('productId:', productId, 'Action:', action) for testing button targeting
        //pulling users authentication from main.html
        console.log('USER:', user)
        // django docs AnonymousUser is presented with non authed user
        if (user === 'AnonymousUser'){
            addCookieItem(productId, action)
        }else {
            updateUserOrder(productId, action)
        }
    })
}

//function for users not authenticated
function addCookieItem(productID, action){
    console.log('User is still not authed')

    //add item
    if (action == 'add'){
        if (cart[productID] == undefined){
            cart[productID] = {'quantity':1}
        }
        else{
            cart[productID]['quantity'] += 1
        }
    }
    //decrease item
    if (action == 'remove'){
        cart[productID]['quantity'] -= 1

        if (cart[productID]['quantity'] <= 0){
            console.log('Remove Item')
            delete cart[productID];
        }
    }
    console.log('Cart', cart)
    // overide the cookie that is set
    document.cookie = 'cart=' + JSON.stringify(cart) + ";domain=;path=/" //cookie from any page
    location.reload()
}

function updateUserOrder(productId, action){
    console.log('User is authenticated, sending data ...')
    //passing in the view function
    var url = '/update_item/'
    //sending product info to backend using fetch api

    fetch(url, {
        method: 'POST',
        headers:{
            'Content-Type':'application/json',
            'X-CSRFToken': csrftoken,
        },//convert object to strings
        body:JSON.stringify({'productId': productId,'action':action})
    })
        .then((response) =>{
            return response.json() //returning the response set in the updateItem view
        })
        .then((data) =>{
           console.log('data:', data)
           location.reload()
        })
}

