from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', include('home.urls')),
    path('admin/', admin.site.urls),
    path('services/', include('store.urls')),
    path('info/', include('Blog.urls')),

]
# appending to the list to the urlpatterns list and pointing media url to media root this allows the display of images
# in the static folder
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# example http://127.0.0.1:8000/images/t-shirt.jpg
