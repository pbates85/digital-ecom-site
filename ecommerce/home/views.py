from django.shortcuts import render
from .forms import ContactForm
# Create your views here.


def homepage(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'success.html')
    form = ContactForm()
    context = {'form': form}
    return render(request, 'homepage.html', context)


def about(request):
    context = {}
    return render(request, 'about.html', context)
