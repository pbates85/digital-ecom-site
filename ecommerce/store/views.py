from posix import posix_spawn
from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
# import the models to provide context to the views
from .models import *
from django.http import JsonResponse
import json
import datetime
# import utils file function
from .utils import cookieCart, cartData, guestOrder
from django.core.mail import send_mail
import os


def store(request):
    # pulled from utils file
    data = cartData(request)
    cartItems = data['cartItems']

    products = Product.objects.all()
    context = {'products': products, 'cartItem': cartItems}
    user = os.getenv('EMAIL_HOST_USER')
    print(user)
    password = os.getenv('EMAIL_HOST_PASSWORD')
    print(password) 
    # send_mail(
    # 'Site Visited',
    # 'User visited',
    # 'itsolutions.trident@gmail.com',
    # ['phillipmbates85@gmail.com'],
    # fail_silently=False,
    # )

    return render(request, 'store/store.html', context)


def cart(request):
    data = cartData(request)

    cartItems = data['cartItems']
    order = data['order']
    items = data['items']
    # now pass items retrieved from the query to the context which will be looped through in the template
    # also passing order as context
    context = {'items': items, 'order': order, 'cartItem': cartItems, 'shipping': False}
    return render(request, 'store/cart.html', context)


def checkout(request):
    data = cartData(request)

    cartItems = data['cartItems']
    order = data['order']
    items = data['items']
    context = {
        'items': items, 'order': order,
        'cartItem': cartItems
    }
    return render(request, 'store/checkout.html', context)


def updateItem(request):
    data = json.loads(request.body)
    productId = data['productId']
    action = data['action']
    print('Action:', action)
    print('Product:', productId)

    customer = request.user.customer
    product = Product.objects.get(id=productId)
    order, created = Order.objects.get_or_create(customer=customer, complete=False)
    orderItem, created = OrderItem.objects.get_or_create(order=order, product=product)

    if action == 'add':
        orderItem.quantity = (orderItem.quantity + 1)
    elif action == 'remove':
        orderItem.quantity = (orderItem.quantity - 1)

    orderItem.save()
    if orderItem.quantity <= 0:
        orderItem.delete()
    return JsonResponse('Item was added', safe=False)


def processOrder(request):
    print('Data:', request.body)
    transaction_id = datetime.datetime.now().timestamp()

    # parse the data
    data = json.loads(request.body)
    # get customer info if authenticated
    if request.user.is_authenticated:
        customer = request.user.customer
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
    # guest logic stored in utils.py
    else:
        customer, order = guestOrder(request, data)

    total = float(data['form']['total'])
    order.transaction_id = transaction_id

    # prevent malicous users from sending an altered json total
    if total == order.get_cart_total:
        order.complete = True
    order.save()

    # create an instance of the shipping adder if it was sent
    if order.shipping == True:
        ShippingAddress.objects.create(
            customer=customer,
            order=order,
            address=data['shipping']['address'],
            city=data['shipping']['city'],
            state=data['shipping']['state'],
            zipcode=data['shipping']['zipcode']
        )

    return JsonResponse('Payment Complete', safe=False)
